package ru.edu;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class CustomLinkedHashMapTests {

    private CustomLinkedHashMap<Integer, String> map = null;// new CustomLinkedHashMapImpl<>(16);

    @Test
    public void sizeTests() {

        Assert.assertEquals(0, map.size());
        Assert.assertTrue(map.isEmpty());

        for (int i = 1; i <= 10; ++i) {
            Assert.assertNull(map.put(i, "Value" + i));
            Assert.assertEquals(i, map.size());
        }

        for (int i = 10; i >= 1; --i) {
            Assert.assertEquals("Value" + i, map.put(i, "NewValue" + i));
        }
        Assert.assertEquals(10, map.size());

        for (int i = 10; i >= 1; --i) {
            Assert.assertEquals("NewValue" + i, map.remove(i));
            Assert.assertEquals(i - 1, map.size());
        }

        Assert.assertTrue(map.isEmpty());
    }

    @Test
    public void getTests() {

        for (int i = 0; i < 10; ++i) {
            Assert.assertNull(map.get(i));
            map.put(i, "V" + i);
            Assert.assertEquals("V" + i, map.get(i));
        }
    }

    @Test
    public void putTests() {

        Assert.assertEquals(0, map.keys().length);

        Assert.assertEquals(null, map.put(10, "Value10"));
        Assert.assertEquals(Arrays.asList(10), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("Value10"), Arrays.asList(map.values()));

        Assert.assertEquals(null, map.put(5, "Value5"));
        Assert.assertEquals(Arrays.asList(10, 5), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("Value10", "Value5"), Arrays.asList(map.values()));

        Assert.assertEquals(null, map.put(15, "Value15"));
        Assert.assertEquals(Arrays.asList(10, 5, 15), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("Value10", "Value5", "Value15"), Arrays.asList(map.values()));

        Assert.assertEquals(null, map.put(null, "null1"));
        Assert.assertEquals("null1", map.put(null, "null2"));
        Assert.assertEquals(Arrays.asList(10, 5, 15, null), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("Value10", "Value5", "Value15", "null2"), Arrays.asList(map.values()));

        Assert.assertEquals("Value15", map.put(15, "Value15"));
        Assert.assertEquals(Arrays.asList(10, 5, 15, null), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("Value10", "Value5", "Value15", "null2"), Arrays.asList(map.values()));

        Assert.assertEquals(null, map.put(7, "Value7"));
        Assert.assertEquals(Arrays.asList(10, 5, 15, null, 7), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("Value10", "Value5", "Value15", "null2", "Value7"), Arrays.asList(map.values()));

        Assert.assertEquals(null, map.put(1, "Value1"));
        Assert.assertEquals(Arrays.asList(10, 5, 15, null, 7, 1), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("Value10", "Value5", "Value15", "null2", "Value7", "Value1"), Arrays.asList(map.values()));

        Assert.assertEquals(null, map.put(9, "Value9"));
        Assert.assertEquals(Arrays.asList(10, 5, 15, null, 7, 1, 9), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("Value10", "Value5", "Value15", "null2", "Value7", "Value1", "Value9"), Arrays.asList(map.values()));

        Assert.assertEquals(null, map.put(20, "Value20"));
        Assert.assertEquals(Arrays.asList(10, 5, 15, null, 7, 1, 9, 20), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("Value10", "Value5", "Value15", "null2", "Value7", "Value1", "Value9", "Value20"), Arrays.asList(map.values()));

        Assert.assertEquals(null, map.put(100, "Value100"));
        Assert.assertEquals(Arrays.asList(10, 5, 15, null, 7, 1, 9, 20, 100), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("Value10", "Value5", "Value15", "null2", "Value7", "Value1", "Value9", "Value20", "Value100"), Arrays.asList(map.values()));

        Assert.assertEquals(9, map.size());
    }

    @Test
    public void removeTests() {

        map.put(10, "Value10");
        Assert.assertEquals(1, map.size());
        Assert.assertTrue(map.containsKey(10));

        map.remove(10);
        Assert.assertFalse(map.containsKey(10));
        Assert.assertEquals(0, map.size());

        map.put(30, "V30");

        map.put(50, "V50");
        map.put(10, "V10");

        map.put(5, "V5");
        map.put(20, "V20");
        map.put(70, "V70");
        map.put(100, "V100");
        map.put(60, "V60");
        map.put(65, "V65");
        map.put(55, "V55");
        map.put(56, "V56");
        map.put(19, "V19");

        Assert.assertEquals(Arrays.asList(30, 50, 10, 5, 20, 70, 100, 60, 65, 55, 56, 19), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("V30", "V50", "V10", "V5", "V20", "V70", "V100", "V60", "V65", "V55", "V56", "V19"), Arrays.asList(map.values()));

        map.remove(20);
        map.remove(100);

        Assert.assertEquals(Arrays.asList(30, 50, 10, 5, 70, 60, 65, 55, 56, 19), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("V30", "V50", "V10", "V5", "V70", "V60", "V65", "V55", "V56", "V19"), Arrays.asList(map.values()));

        map.remove(65);

        Assert.assertEquals(Arrays.asList(30, 50, 10, 5, 70, 60, 55, 56, 19), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("V30", "V50", "V10", "V5", "V70", "V60", "V55", "V56", "V19"), Arrays.asList(map.values()));

        map.remove(50);
        Assert.assertEquals(Arrays.asList(30, 10, 5, 70, 60, 55, 56, 19), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("V30", "V10", "V5", "V70", "V60", "V55", "V56", "V19"), Arrays.asList(map.values()));

        map.remove(30);
        Assert.assertEquals(Arrays.asList(10, 5, 70, 60, 55, 56, 19), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList("V10", "V5", "V70", "V60", "V55", "V56", "V19"), Arrays.asList(map.values()));

        map.remove(5);
        map.remove(10);
        map.remove(70);
        map.remove(19);
        map.remove(55);
        map.remove(56);
        map.remove(60);

        Assert.assertEquals(0, map.size());
        Assert.assertEquals(Arrays.asList(), Arrays.asList(map.keys()));
        Assert.assertEquals(Arrays.asList(), Arrays.asList(map.values()));
    }

    @Test
    public void containsKeyTests() {

        for (int i = 0; i < 10; ++i) {
            map.put(i, "V1");
            Assert.assertTrue(map.containsKey(i));
        }
        map.put(null, "Vnull");

        Assert.assertTrue(map.containsKey(null));
        Assert.assertFalse(map.containsKey(1000));
    }

    @Test
    public void containsValueTests() {

        for (int i = 0; i < 10; ++i) {
            map.put(i, "V" + i);
            Assert.assertTrue(map.containsValue("V" + i));
            Assert.assertTrue(map.containsKey(i));
        }
        map.put(null, "Vnull");

        Assert.assertTrue(map.containsValue("Vnull"));
        Assert.assertFalse(map.containsValue("V1000"));
    }

    @Test
    public void keysTests() {

        map.put(10, "Value10");
        Assert.assertEquals(1, map.size());

        map.remove(10);
        Assert.assertFalse(map.containsKey(10));

        map.put(30, "V30");
        map.put(null, "Vnull");

        map.put(50, "V50");
        map.put(10, "V10");

        map.put(5, "V5");
        map.put(20, "V20");
        map.put(70, "V70");
        map.put(100, "V100");
        map.put(60, "V60");
        map.put(65, "V65");
        map.put(55, "V55");
        map.put(56, "V56");
        map.put(19, "V19");

        Assert.assertEquals(Arrays.asList(30, null, 50, 10, 5, 20, 70, 100, 60, 65, 55, 56, 19), Arrays.asList(map.keys()));
    }

    @Test
    public void valuesTests() {

        map.put(10, "Value10");
        Assert.assertEquals(1, map.size());
        Assert.assertTrue(map.containsKey(10));

        map.remove(10);
        Assert.assertFalse(map.containsKey(10));
        Assert.assertEquals(0, map.size());

        map.put(30, "V30");

        map.put(50, "V50");
        map.put(10, "V10");

        map.put(5, "V5");
        map.put(20, "V20");
        map.put(70, "V70");
        map.put(100, "V100");
        map.put(60, "V60");
        map.put(65, "V65");
        map.put(55, "V55");
        map.put(56, "V56");
        map.put(19, "V19");

        Assert.assertEquals(Arrays.asList("V30", "V50", "V10", "V5", "V20", "V70", "V100", "V60", "V65", "V55", "V56", "V19"), Arrays.asList(map.values()));
    }
}
