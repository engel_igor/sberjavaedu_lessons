#HW_L8_T3_CustomLinkedHashMapImpl

Реализовать класс CustomLinkedHashMapImpl<K, V>, который представляет отображение на основе хеш-таблицы.

Класс CustomLinkedHashMapImpl реализует интерфейс CustomLinkedHashMap<K, V>
Класс CustomLinkedHashMapImpl может хранить объекты любого типа
Методы toString & keys должны возвращать элементы в том порядке, в котором элементы были добавлены в отображение

##Конструкторы

CustomLinkedHashMapImpl();

Ссылка на CustomLinkedHashMap<K, V>: #ref

#Критерии приемки

1. Создать ветку feature/CustomLinkedHashMap
2. Добавить интерфейс CustomLinkedHashMap в ветку, сделать PUSH в удаленный репозиторий

3. Создать ветку feature/CustomLinkedHashMapImpl от ветки feature/CustomLinkedHashMap

4. Написать реализацию класса CustomLinkedHashMapImpl

5. Предоставить на проверку Pull Request из ветки feature/CustomLinkedHashMapImpl в ветку feature/CustomLinkedHashMap

6. Каждый публичный метод класса CustomLinkedHashMapImpl должен быть покрыт unit тестом

7. !!! Вносить правки в интерфейс CustomLinkedHashMap<K, V> нельзя

