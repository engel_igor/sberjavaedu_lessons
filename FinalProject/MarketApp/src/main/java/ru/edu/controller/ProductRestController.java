package ru.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.service.ProductInfo;
import ru.edu.service.ProductService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductRestController {

    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    /**
     * Get all.
     *
     * @author -  Давлитшин Ринат
     */
    public List<ProductInfo> getAll() {
        return new ArrayList<>();
    }

    /**
     * Get products of current category with similar prices.
     *
     * @author -  Давлитшин Ринат
     */
    public List<ProductInfo> getSimilarProducts(String itemId, double priceDelta) {
        return new ArrayList<>();
    }

    /**
     * Get product by id. Returns null if not found.
     *
     * @param itemId - item id
     * @author - Михайлова Ольга
     */
    public ProductInfo getProduct(String itemId) {
        return null;
    }

    /**
     * Create new product.
     *
     * @author - Ляшенко Максим
     */
    public ProductInfo create(ProductInfo info) {
        return null;
    }

    /**
     * Update existing product. Don't change id
     *
     * @author - Ляшенко Максим
     */
    public ProductInfo update(ProductInfo info) {
        return null;
    }

    /**
     * Delete product by id
     *
     * @author - Кочнев Виталий
     */
    public ProductInfo delete(String itemId) {
        return null;
    }
}
