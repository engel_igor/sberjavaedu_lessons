package ru.edu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.edu.dao.ProductRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ProductService {

    private ProductRepository repository;

    @Autowired
    public void setRepository(ProductRepository repository) {
        this.repository = repository;
    }

    /**
     * Get all.
     *
     * @author -  Давлитшин Ринат
     */
    public List<ProductInfo> getAll() {
        return new ArrayList<>();
    }

    /**
     * Get products of current category with similar prices.
     *
     * @throws ru.edu.error.CustomException with code PRODUCT_NOT_FOUND if product doesn't exist
     * @throws ru.edu.error.CustomException with code ARGUMENT_ERROR if priceDelta is negative
     * @author -  Давлитшин Ринат
     */
    public List<ProductInfo> getSimilarProducts(String itemId, double priceDelta) {
        return new ArrayList<>();
    }

    /**
     * Get product by id. Returns null if not found.
     *
     * @param itemId - item id
     * @throws ru.edu.error.CustomException with code PRODUCT_NOT_FOUND if product doesn't exist
     * @author - Михайлова Ольга
     */
    public ProductInfo getProduct(String itemId) {
        return null;
    }

    /**
     * Create new product.
     *
     * @throws ru.edu.error.CustomException with code PRODUCT_ALREADY_EXISTS if product with current id already exists
     * @throws ru.edu.error.CustomException with code ARGUMENT_ERROR if info is incorrect
     * @author - Ляшенко Максим
     */
    public ProductInfo create(ProductInfo info) {
        return null;
    }

    /**
     * Update existing product. Don't change id
     *
     * @throws ru.edu.error.CustomException with code PRODUCT_NOT_FOUND if product doesn't exist
     * @throws ru.edu.error.CustomException with code ARGUMENT_ERROR if info is incorrect
     * @author - Ляшенко Максим
     */
    public ProductInfo update(ProductInfo info) {
        return null;
    }

    /**
     * Delete product by id
     *
     * @throws ru.edu.error.CustomException with code PRODUCT_NOT_FOUND if product doesn't exist
     * @author - Кочнев Виталий
     */
    public ProductInfo delete(String itemId) {
        return null;
    }

    /**
     * Categories. Return constants.
     *
     * @author - Кочнев Виталий
     */
    public Map<String, CategoryInfo> getCategories() {
        return new HashMap<>();
    }
}
