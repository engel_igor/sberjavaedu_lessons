package ru.edu.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.edu.service.ProductInfo;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProductRepository {

    private Connection connection;

    @Autowired
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * Get all.
     *
     * @author -  Давлитшин Ринат
     */
    public List<ProductInfo> getAll() {
        return new ArrayList<>();
    }

    /**
     * Get product by id. Returns null if not found.
     *
     * @param itemId - item id
     * @author - Михайлова Ольга
     */
    public ProductInfo getProduct(String itemId) {
        return null;
    }

    /**
     * Create new product.
     *
     * @author - Ляшенко Максим
     */
    public ProductInfo create(ProductInfo info) {
        return null;
    }

    /**
     * Update existing product. Don't change id
     *
     * @author - Ляшенко Максим
     */
    public ProductInfo update(ProductInfo info) {
        return null;
    }

    /**
     * Delete product by id.
     *
     * @author - Кочнев Виталий
     */
    public ProductInfo delete(String itemId) {
        return null;
    }
}
